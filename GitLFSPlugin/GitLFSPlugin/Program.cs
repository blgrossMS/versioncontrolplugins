﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLFSPlugin
{
    class Program
    {
        static int Main(string[] args)
        {
            System.Diagnostics.Debugger.Launch();
            return Run();
        }

        static int Run()
        {
            GitTask task = new GitTask(null, null, string.Empty);
            return task.Run();
        }
    }
}
