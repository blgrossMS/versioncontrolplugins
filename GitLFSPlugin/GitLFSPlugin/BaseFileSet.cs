﻿
namespace GitLFSPlugin
{
    public class BaseFileSetRequest : BaseRequest
    {
        public BaseFileSetRequest()
            : base(null, null)
        {

        }

        public BaseFileSetRequest(CommandArgs args, Connection conn)
            : base(args, conn)
        {
        }
    }

    public class BaseFileSetResponse<Req> : BaseResponse where Req : BaseFileSetRequest
    {

        public BaseFileSetResponse(Req req)
        {
            this.request = req;
            this.conn = req.Conn;
        }

        public override void Write()
        {
            if (request.Invalid)
                return;
            
            conn.EndResponse();
        }

        Req request;
        Connection conn;
    }
}
