﻿using System;
using System.Reflection;
using System.Diagnostics;

namespace GitLFSPlugin
{
    class GitTask : IDisposable
    {
        Connection connection;
        string projectPath;
        string tpcPath;

        public GitTask(Connection connection, string tpcPath, string projectPath)
        {
            this.connection = connection;
            this.tpcPath = tpcPath;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Close();
            }
        }

        public void Close()
        {
            if (this.connection != null)
            {
                this.connection.Close();
                this.connection = null;
            }
        }

        public int Run()
        {
            if (this.connection == null)
            {
                connection = new Connection("./Library/gitlfsplugin.log");
            }

            UnityCommand cmd;
            CommandArgs commandArgs = new CommandArgs();

            try
            {
                this.connection.LogInfo("\nBegin Connection - " + DateTime.Now.ToString() + " v" + GetAssemblyFileVersion());

                while (true)
                {
                    cmd = this.connection.ReadCommand(commandArgs);

                    if (cmd == UnityCommand.UCOM_Invalid)
                    {
                        return 1; // error
                    }
                    else if (cmd == UnityCommand.UCOM_Shutdown)
                    {
                        this.connection.EndResponse();
                        return 0; // ok 
                    }
                    else
                    {
                        Dispatch(this.connection, this, cmd, commandArgs);
                    }
                }
            }
            catch (Exception e)
            {
                this.connection.WarnLine(("Fatal: " + e.ToString()));
            }

            return 1;
        }

        public static bool Dispatch(Connection conn, GitTask session, UnityCommand cmd, CommandArgs args)
        {
            conn.LogInfo(args[0] + "::Run()");

            var timer = new Stopwatch();
            timer.Start();

            try
            {
                switch (cmd)
                {
                    case UnityCommand.UCOM_Config:
                        return RunCommand<GitConfigCommand, ConfigRequest, ConfigResponse, GitTask>(conn, session, args);
                    case UnityCommand.UCOM_Status:
                        return RunCommand<GitStatusCommand, BaseFileSetRequest, BaseFileSetResponse<BaseFileSetRequest>, GitTask>(conn, session, args);
                    default:
                        break;
                }
            }
            finally
            {
                conn.LogDebug("\n------------ " + timer.Elapsed.ToString() + " --------------\n", false);
            }

            return false;
        }

        public static bool RunCommand<Cmd, Req, Resp, Sess>(Connection conn, Sess session, CommandArgs args)

            where Req : BaseRequest
            where Resp : BaseResponse
            where Cmd : ICommand<Sess, Req, Resp>, new()
        {
            Req req = (Req)Activator.CreateInstance(typeof(Req), args, conn);

            // Invalid requests will send warnings/error to unity and we can just continue.
            // If something fatal happens an exception is thrown when reading the request.
            if (req.Invalid) return true;

            Resp resp = (Resp)Activator.CreateInstance(typeof(Resp), req);

            Cmd found = new Cmd();

            try
            {
                return found.Run(session, req, resp);
            }
            catch (Exception ex)
            {
                conn.WarnLine(ex.Message);
                conn.Log(ex.ToString());

                resp.Write();
            }

            return false;
        }

        public static string GetAssemblyFileVersion()
        {
            var assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.FileVersion;
        }
    }
}
