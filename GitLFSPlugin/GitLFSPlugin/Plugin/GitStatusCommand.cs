﻿using Microsoft.Alm.GitProcessManagement;

namespace GitLFSPlugin
{
    class GitStatusCommand : ICommand<GitTask, BaseFileSetRequest, BaseFileSetResponse<BaseFileSetRequest>>
    {
        public bool Run(GitTask task, BaseFileSetRequest req, BaseFileSetResponse<BaseFileSetRequest> resp)
        {
            bool recursive = req.Args.Count > 1 && req.Args[1] == "recursive";
            bool offline = req.Args.Count > 2 && req.Args[2] == "offline";

            Repository repo = Repository.Open()

            // do a git status here and write the results to unity

            resp.Write();
            return true;
        }
    }
}
